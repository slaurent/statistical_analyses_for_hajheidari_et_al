# statistical_analyses_for_Hajheidari_et_al
this repository contains the analyses provided by Stefan Laurent for the article: [Autoregulation of RCO by Low-Affinity Binding Modulates Cytokinin Action and Shapes Leaf Diversity ](https://www.cell.com/current-biology/pdfExtended/S0960-9822(19)31380-6)
